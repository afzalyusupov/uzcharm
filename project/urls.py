from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import gettext_lazy as _
from users import views as user_view
import users


urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', include('blogs.urls')),
    path('user/', include('users.urls')),
)

urlpatterns += [
    path('rosetta/', include('rosetta.urls')),
    path('captcha/', include('captcha.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

admin.site.site_header = "Uzcharmsanoat admin panel"
admin.site.site_title = "Uzcharmsanoat Admin Portal"
admin.site.index_title = "Welcome to Uzcharmsanoat Portal"
