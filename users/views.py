from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views import View
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.http import JsonResponse

from django.contrib import messages
from django.core.mail import EmailMessage
from django.contrib import auth
from django.contrib.auth.decorators import login_required

from django.utils.encoding import force_bytes, force_text, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
import json
from validate_email import validate_email

from .forms import ProfileForm
from .utils import token_generator
from .models import User, Message
from django.db.models import Q

import json


@login_required
def chatroom(request, pk: int):
    other_user = get_object_or_404(User, pk=pk)
    messages = Message.objects.filter(
        Q(receiver=other_user, sender=request.user)
    )
    messages.update(seen=True)
    messages = messages | Message.objects.filter(Q(receiver=other_user, sender=request.user))
    return render(request, "users/chatroom.html", {"other_user": other_user, "messages": messages})


@login_required
def ajax_load_messages(request, pk):
    other_user = get_object_or_404(User, pk=pk)
    messages = Message.objects.filter(seen=False).filter(
        Q(receiver=request.user, sender=other_user)
    )
    messages.update(seen=True)
    message_list = [{
        "sender": message.sender.username,
        "message": message.message,
        "sent": message.sender == request.user
    } for message in messages]
    if request.method == "POST":
        message = json.loads(request.body)
        m = Message.objects.create(receiver=other_user, sender=request.user, message=message)
        message_list.append(
            {
                "sender": request.user.username,
                "message": m.message,
                "sent": True,
            })
        return JsonResponse(message_list, safe=False)


class RegistrationView(View):
    def get(self, request):
        return render(request, 'users/register_2.html')

    def post(self, request):
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        context = {
            'fieldValues': request.POST
        }

        """
        try:
            u = User.objects.create_user(self.format_phone_number(self.phone_number), password=self.form['password'])
        except Exception:
            pass
        self.base_user = u

        return super().save(force_insert, force_update, using, update_fields)

        """

        if not User.objects.filter(username=username).exists():
            if not User.objects.filter(email=email).exists():
                if len(password) < 6:
                    messages.error(request, 'Password is to short')
                    return render(request, 'users/register_2.html', context)

                user = User.objects.create_user(username=username, email=email)
                user.set_password(password)
                user.is_active = False
                user.save()

                # path_to_view
                uidb64 = urlsafe_base64_encode(force_bytes(user.pk))

                domain = get_current_site(request).domain
                link = reverse('activate', kwargs={'uidb64': uidb64, 'token': token_generator.make_token(user)})

                activate_url = "http://"+domain+link

                email_subject = "Activate your account"
                email_body = "Hi " + user.username + " ,Please use this link to activate your account\n" + activate_url
                email = EmailMessage(
                    email_subject,
                    email_body,
                   'noreply@semycolon.com',
                    [email],
                )

                email.send(fail_silently=False)
                messages.success(request, 'Account successfully created, check your email')
                return render(request, 'users/register_2.html')

        return render(request, 'users/register_2.html')

    def format_phone_number(self, raw_phone_number):
        return str(raw_phone_number).replace('+', '').replace('-', '').replace(' ', '').replace('(', '').replace(')', '')


class EmailValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        if not validate_email(email):
            return JsonResponse({'email error': 'Email is invalid'}, status=400)
        if User.objects.filter(email=email).exists():
            return JsonResponse({'email error': 'sorry email is already taken'}, status=409)
        return JsonResponse({'email_valid': True})


class UsernameValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        username = data['username']
        if not str(username).isalnum():
            return JsonResponse({'username error': 'username should only contain alphanumeric chars'}, status=400)
        if User.objects.filter(username=username).exists():
            return JsonResponse({'username error': 'sorry username is already taken'}, status=409)
        return JsonResponse({'username_valid': True})


class VerificationView(View):
    def get(self, request, uidb64, token):
        try:
            id = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not token_generator.check_token(user, token):
                return redirect('login'+'?message='+'User already activated')

            if user.is_active:
                return redirect('login')
            user.is_active = True
            user.save()

            messages.success(request, 'Account activated successfully')
            return redirect('login')

        except Exception as ex:
            pass

        return redirect('login')


class LoginView(View):
    def get(self, request):
        return render(request, "users/login_2.html")

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']

        if username and password:
            user = auth.authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    auth.login(request, user)
                    messages.success(request, 'Welcome, ' + user.username + '. You are logged in')
                    return redirect('blogs:index')

                messages.error(request, 'Account is not active, please check your email')
                return render(request, "users/login_2.html")

            messages.error(request, 'Invalid credentials, try again')
            return render(request, "users/login_2.html")

        messages.error(request, 'Please fill in all fields')
        return render(request, "users/login_2.html")


class LogoutView(View):
    def get(self, request):
        auth.logout(request)
        messages.success(request, 'You have logged out')
        return redirect('blogs:index')


@login_required
def profile(request):
    form = ProfileForm()

    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user = User.objects.get(pk=request.user.pk)
            user.first_name = form.data.get('first_name')
            user.last_name = form.data.get('last_name')
            user.save()

    return render(request, 'users/profile.html', {'form': form})
