from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model


class Message(models.Model):
    sender = models.ForeignKey(User, related_name="sent_messages", on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name="received_messages", on_delete=models.CASCADE)
    message = models.TextField()
    seen = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("date_created",)


class Profile(models.Model):
    base_user = models.OneToOneField(User, on_delete=models.CASCADE)
    name_of_organization = models.CharField(max_length=255, blank=True, null=True, unique=True)
    address_of_organization = models.CharField(max_length=255, blank=True, null=True, unique=True)
    organizational_legal_form = models.CharField(max_length=255, blank=True, null=True)
    type_of_activity = models.CharField(max_length=255, blank=True, null=True)
    date_of_registration = models.DateField()
    launch_date = models.DateField()
    authorized_capital = models.CharField(max_length=200, blank=True, null=True)
    share_of_foreign_capital = models.CharField(max_length=255, blank=True, null=True)
    company_founders = models.CharField(max_length=255, blank=True, null=True)
    foreign_founders = models.CharField(max_length=255, blank=True, null=True)
    total_area = models.CharField(max_length=200, blank=True, null=True)
    production_area = models.CharField(max_length=200, blank=True, null=True)
    types_of_producing_products = models.CharField(max_length=200, blank=True, null=True)
    main_production_indicators = models.CharField(max_length=200, blank=True, null=True)
    annual_revenue = models.CharField(max_length=200, blank=True, null=True)
    number_of_employees = models.CharField(max_length=200, blank=True, null=True)
    export_volume = models.CharField(max_length=200, blank=True, null=True)
    import_volume = models.CharField(max_length=200, blank=True, null=True)
    stir_code = models.CharField(max_length=200, blank=True, null=True)
    okpo_code = models.CharField(max_length=200, blank=True, null=True)
    bank_details = models.CharField(max_length=200, blank=True, null=True, unique=True)
    web_site = models.CharField(max_length=200, blank=True, null=True, unique=True)
    email = models.CharField(max_length=200, blank=True, null=True, unique=True)
    financial_indicators = models.CharField(max_length=200, blank=True, null=True)
    head_of_organization = models.CharField(max_length=200, blank=True, null=True)
    phone_number = models.CharField(max_length=20, blank=True, null=True, unique=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # self.base_user =
        super().save(force_insert, force_update, using, update_fields)

    def __str__(self):
        return self.name_of_organization
