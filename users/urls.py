from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    path('register/', views.RegistrationView.as_view(), name="register"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('profile/', views.profile, name="profile"),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('validate-username/', csrf_exempt(views.UsernameValidationView.as_view()), name='validate-username'),
    path('validate-email/', csrf_exempt(views.EmailValidationView.as_view()), name='validate-email'),
    path('activate/<uidb64>/<token>', views.VerificationView.as_view(), name="activate"),
    path('<int:pk>/', views.chatroom, name="chatroom"),
    path('ajax/<int:pk>/', views.ajax_load_messages, name="chatroom-ajax"),
]
