from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from users.models import Profile


class SignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'username')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = (
            'name_of_organization',
            'address_of_organization',
            'organizational_legal_form',
            'type_of_activity',
            'date_of_registration',
            'launch_date',
            'authorized_capital',
            'share_of_foreign_capital',
            'company_founders',
            'foreign_founders',
            'total_area',
            'production_area',
            'types_of_producing_products',
            'main_production_indicators',
            'annual_revenue',
            'number_of_employees',
            'export_volume',
            'import_volume',
            'stir_code',
            'okpo_code',
            'bank_details',
            'web_site',
            'email',
            'financial_indicators',
            'head_of_organization',
            'phone_number',
        )
