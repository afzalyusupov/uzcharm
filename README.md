Instructions to setup the project:

1. Clone the repository from gitlab with the following command: 
    ```
    git clone https://gitlab.com/afzalyusupov/uzcharm.git
    ```

2. Open the directory, which was cloned from gitlab in your preferred IDE.

3. After opening the directory in your IDE, run the following commands to install all required dependencies:
    ```
    pip install -r requirements.txt

    ```

4. Once pip has finished downloading the dependencies, run the following commands in your terminal:
    ```
    python manage.py makemigrations
    python manage.py migrate
    python manage.py createsuperuser
    ```

5. To start the local server run the following command: 
    ```
    python manage.py runserver
    ```

6. Open your browser and navigate to http://127.0.0.1:8000/

7. To access the admin panel, navigate to http://127.0.0.1:8000/admin in your browser.
