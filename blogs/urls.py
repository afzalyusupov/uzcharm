from django.urls import path
from django.utils.translation import gettext_lazy as _
from . import views
from blogs import views as contact_views

app_name = 'blogs'
urlpatterns = [
    path('', views.index_page, name='index'),
    path(_('about/'), views.about_page, name='about'),

    path(_('news/'), views.NewsListView.as_view(), name='news_list'),
    path(_('news/<int:year>/<int:month>/<int:day>/<slug:slug>'), views.NewsDetailView.as_view(), name='news_detail'),

    path(_('tender/'), views.TenderListView.as_view(), name='tender_list'),
    path(_('tender/<int:year>/<int:month>/<int:day>/<slug:slug>'), views.TenderDetailView.as_view(),
         name='tender_detail'),

    path(_('product/'), views.ProductListView.as_view(), name='product_list'),

    path(_('membership/'), views.MembershipListView.as_view(), name='membership_list'),
    path(_('membership/<slug:slug>'), views.MembershipDetailView.as_view(), name='membership_detail'),

    path(_('exhibitions/'), views.ExhibitionListView.as_view(), name='exhibition'),
    path(_('exhibitions/<int:year>/<int:month>/<int:day>/<slug:slug>'), views.ExhibitionDetailView.as_view(),
         name='exhibition_detail'),

    path(_('gallery/'), views.GalleryListView.as_view(), name='gallery'),

    # path(_('gallery/'), views.Gallery, name='gallery'),

    path(_('media/'), views.MediaListView.as_view(), name='media'),

    path(_('vacancy/'), views.VacancyListView.as_view(), name='vacancy'),

    path(_('faq/'), views.FAQListView.as_view(), name='faq'),

    path(_('contact/'), contact_views.contact_view, name='contact_form'),

    path(_('virtual_reception/'), views.virtual_reception_view, name='virtual_reception_form'),

    path(_('entrepreneurs/'), views.EntrepreneursListView.as_view(), name='entrepreneurs'),

    path(_('membership_procedure/'), views.MembershipProcedureListView.as_view(), name='membership_procedure'),

    path(_('benefits/)'), views.BenefitListView.as_view(), name='benefits'),

    path(_('content/<slug:slug>'), views.StaticContentView.as_view(), name='static_page'),

    # path(_('legal/<slug:slug>'), views.LegalDocumentView.as_view(), name='legal'),

    path(_('legal/'), views.LegalDocumentListView.as_view(), name='legal'),

    path(_('legal/<slug:slug>'), views.LegalDocumentView.as_view(), name='legal_detail'),

    path(_('statistic/'), views.StatisticListView.as_view(), name='statistic_list'),

    path(_('statistic/<slug:slug>'), views.StatisticDetailView.as_view(), name='statistic_detail'),

]
